import React from 'react';
import classnames from 'classnames';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const creator = [
  {
    title: <>Engineer</>,
    imageUrl: 'img/aiengineer.jpg',
    issueURL: 'https://gitlab.com/iotiotdotin/covid19ai/engineer',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  }, 
];

function Feature({imageUrl, title, issueURL, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={classnames('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <div className="container">
        <h3>{title}</h3>
        <p>{description}</p>
        <div className={styles.buttons}>
          <a href={issueURL}>
            <div
              className={classnames(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}>
              Contribute
            </div>
          </a>
        </div>
      </div>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="CovidAI <head />">
      <header className={classnames('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
        </div>
      </header>
      <main>
        <div className={classnames('hero hero--primary-dark', styles.heroBanner)}>
          <div className="container">
            <h1 className="hero__title">Want to contribute?</h1>
            <p className="hero__subtitle">Please pick one Role that suits you, if you can do multiple things, you will find different tasks under different roles.</p>
          </div>
        </div>
        {creator && creator.length && (
          <section className={styles.creator}>
            <div className={classnames('hero hero--primary-darkest', styles.headerBanner)}>
              <div className="container">
                <h1 className="hero__title">Creators</h1>
              </div>
            </div>
            <div className="container">
              <div className="row">
                {creator.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
        
        <div className={classnames('hero hero--primary-dark', styles.heroBanner)}>
          <div className="container">
            <h1 className="hero__title">Confused to get started ?</h1>
            <p className="hero__subtitle">Read a guide</p>
            <div className={styles.buttons}>
              <a href="https://gitlab.com/iotiotdotin/covid19ai/welcome-to-convidai/-/wikis/home">
                <div
                  className={classnames(
              		'button button--outline button--secondary button--lg',
              		styles.indexCtas,
                  )}>
                  Read Contributor Guide
                </div>
              </a>
            </div>
          </div>
        </div>
      </main>
    </Layout>
  );
}

export default Home;
