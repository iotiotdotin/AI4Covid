module.exports = {
  title: 'CovidAI',
  tagline: 'Open Source AI project to detect images with COVID-19 infection.',
  url: 'https://iotiotdotin.gitlab.io',
  baseUrl: '/AI4Covid/',
  favicon: 'img/favicon.ico',
  organizationName: 'iotiotdotin', // Usually your GitHub org/user name.
  projectName: 'AI4Covid', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'CovidAI',
      logo: {
        alt: 'CovidAI Logo',
        src: 'img/logo.png',
      },
      links: [
        {to: 'blog', label: 'Blog', position: 'left'},
	{to: 'docs/doc1', label: 'Wiki', position: 'left'},
        {
          href: 'https://gitter.im/covidai/community',
          label: 'Chat',
          position: 'right',
        },
        {
          href: 'https://gitlab.com/iotiotdotin/covid19ai/resources/-/wikis/home',
          label: 'Resources',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Community',
          items: [
            {
              label: 'Gitter',
              href: 'https://gitter.im/covidai/community',
            },
          ],
        },
        {
          title: 'Social',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/iotiotdotin/covid19ai/covidai',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} IoTIoT.in.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
